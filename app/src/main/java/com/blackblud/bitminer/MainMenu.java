package com.blackblud.bitminer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Scanner;

public class MainMenu extends AppCompatActivity implements View.OnClickListener{

    GoogleSignInClient mGoogleSignInClient;
    int RC_SIGN_IN = 0;
    TextView userName, ver_sign;
    ImageView userPicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        LinearLayout StartGameButton = findViewById(R.id.start_game_btn);
        LinearLayout SettingsButton = findViewById(R.id.settings_btn);
        LinearLayout ExitButton = findViewById(R.id.exit_btn);
        LinearLayout GoogleButton = findViewById(R.id.sign_google_btn);
        LinearLayout FacebookButton = findViewById(R.id.sign_facebook_btn);

        StartGameButton.setOnClickListener(this);
        SettingsButton.setOnClickListener(this);
        ExitButton.setOnClickListener(this);
        GoogleButton.setOnClickListener(this);
        FacebookButton.setOnClickListener(this);

        ver_sign = findViewById(R.id.version_sign);
        ver_sign.setOnClickListener(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        userName = findViewById(R.id.username);
        userPicture = findViewById(R.id.userpic);

        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
        if (acct != null) {
            String personName = acct.getDisplayName();
            Uri personPhoto = acct.getPhotoUrl();

            userName.setText(personName);

            Picasso.with(MainMenu.this).load(personPhoto.toString())
                    .into(userPicture);

        }

        askForPermition();
    }

    private void askForPermition() {

        PermissionListener PermList = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                File folder = new File(Data.path);
                if (!folder.exists()) {
                    folder.mkdirs();
                }
                //Toast.makeText(MainMenu.this, "Permission granted", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                //Toast.makeText(MainMenu.this, "Permission not given", Toast.LENGTH_SHORT).show();
            }
        };

        TedPermission.with(MainMenu.this)
                .setPermissionListener(PermList)
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .check();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onClick(View v) {
        MediaPlayer mp_click = MediaPlayer.create(this, R.raw.click);
        switch(v.getId()){
            case R.id.start_game_btn:
                mp_click.start();

                File file_check = new File (Data.path + "/SaveFile1.txt");
                if (file_check.length() == 0){
                    Data.level = 1;
                    Data.progress = 1;
                    Data.balance = 99975;
                    Data.hash = 1;
                    Data.auto_hash = 0;

                    Data.current_os = 0;
                    Data.current_motherboard = 0;
                    Data.current_cpu = 0;
                    Data.current_ram = 0;
                    Data.current_gpu = 0;
                    Data.current_network = 0;

                    Data.current_os_price = 0;
                    Data.current_motherboard_price = 0;
                    Data.current_cpu_price = 0;
                    Data.current_ram_price = 0;
                    Data.current_gpu_price = 0;
                    Data.current_network_price = 0;

                    Data.current_asic_1_1 = 0;
                    Data.current_asic_1_1_price = 0;
                    Data.current_asic_1_2 = 0;
                    Data.current_asic_1_2_price = 0;
                    Data.current_asic_1_3 = 0;
                    Data.current_asic_1_3_price = 0;
                    Data.current_asic_2_1 = 0;
                    Data.current_asic_2_1_price = 0;
                    Data.current_asic_2_2 = 0;
                    Data.current_asic_2_2_price = 0;
                    Data.current_asic_2_3 = 0;
                    Data.current_asic_2_3_price = 0;

                    Data.music = 1;
                    Data.sfx = 1;
                } else {
                    String data = "";

                    try {
                        Scanner reader = new Scanner(file_check);
                        while (reader.hasNextLine()) {
                            data = reader.nextLine();
                            System.out.println(data);

                        }
                        reader.close();
                    } catch (FileNotFoundException e) {
                        StringWriter errors = new StringWriter();
                        e.printStackTrace(new PrintWriter(errors));

                        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
                        if (acct != null) {
                            JavaMailAPI javaMailAPI = new JavaMailAPI(this, "jur20soniic@gmail.com", "Bug Report [" + acct.getEmail() + "]", errors.toString());
                            javaMailAPI.execute();
                        } else {
                            JavaMailAPI javaMailAPI = new JavaMailAPI(this, "jur20soniic@gmail.com", "Bug Report [Anonymous]", errors.toString());
                            javaMailAPI.execute();
                        }
                    }

                    String [] values = data.split("\\|");

                    Data.level = Integer.parseInt(values[0]);
                    Data.progress = Integer.parseInt(values[1]);
                    Data.balance = Integer.parseInt(values[2]);
                    Data.hash = Integer.parseInt(values[3]);
                    Data.auto_hash = Integer.parseInt(values[4]);

                    Data.current_os = Integer.parseInt(values[5]);
                    Data.current_motherboard = Integer.parseInt(values[6]);
                    Data.current_cpu = Integer.parseInt(values[7]);
                    Data.current_ram = Integer.parseInt(values[8]);
                    Data.current_gpu = Integer.parseInt(values[9]);
                    Data.current_network = Integer.parseInt(values[10]);

                    Data.current_os_price = Integer.parseInt(values[11]);
                    Data.current_motherboard_price = Integer.parseInt(values[12]);
                    Data.current_cpu_price = Integer.parseInt(values[13]);
                    Data.current_ram_price = Integer.parseInt(values[14]);
                    Data.current_gpu_price = Integer.parseInt(values[15]);
                    Data.current_network_price = Integer.parseInt(values[16]);

                    Data.current_asic_1_1 = Integer.parseInt(values[17]);
                    Data.current_asic_1_1_price = Integer.parseInt(values[18]);
                    Data.current_asic_1_2 = Integer.parseInt(values[19]);
                    Data.current_asic_1_2_price = Integer.parseInt(values[20]);
                    Data.current_asic_1_3 = Integer.parseInt(values[21]);
                    Data.current_asic_1_3_price = Integer.parseInt(values[22]);
                    Data.current_asic_2_1 = Integer.parseInt(values[23]);
                    Data.current_asic_2_1_price = Integer.parseInt(values[24]);
                    Data.current_asic_2_2 = Integer.parseInt(values[25]);
                    Data.current_asic_2_2_price = Integer.parseInt(values[26]);
                    Data.current_asic_2_3 = Integer.parseInt(values[27]);
                    Data.current_asic_2_3_price = Integer.parseInt(values[28]);

                    Data.music = Integer.parseInt(values[29]);
                    Data.sfx = Integer.parseInt(values[30]);
                }

                try {
                    Data.SaveData();
                } catch (IOException e) {
                    StringWriter errors = new StringWriter();
                    e.printStackTrace(new PrintWriter(errors));

                    GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
                    if (acct != null) {
                        JavaMailAPI javaMailAPI = new JavaMailAPI(this, "jur20soniic@gmail.com", "Bug Report [" + acct.getEmail() + "]", errors.toString());
                        javaMailAPI.execute();
                    } else {
                        JavaMailAPI javaMailAPI = new JavaMailAPI(this, "jur20soniic@gmail.com", "Bug Report [Anonymous]", errors.toString());
                        javaMailAPI.execute();
                    }
                }

                startActivity(new Intent(MainMenu.this, MainActivity.class));
                overridePendingTransition(0, R.anim.slide_out_bottom);
                finish();
                break;
            case R.id.settings_btn:
                mp_click.start();
                startActivity(new Intent(MainMenu.this, Settings.class));
                overridePendingTransition(0, R.anim.slide_out_bottom);
                finish();
                break;
            case R.id.exit_btn:
                mp_click.start();
                finish();
                System.exit(0);
                break;
            case R.id.sign_google_btn:
                mp_click.start();
                signIn();
                break;
            case R.id.sign_facebook_btn:
                mp_click.start();
                Toast.makeText(this, "Facebook login is not available yet.", Toast.LENGTH_LONG).show();
                break;
            case R.id.version_sign:
                mp_click.start();
                SignOut();
                break;
        }
    }

    private void SignOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        userName.setText("UserID");
                        userPicture.setImageResource(R.color.pic_gray);
                        if(ver_sign.getText() == "Sign Out"){
                            Toast.makeText(MainMenu.this, "Successfully signed out.", Toast.LENGTH_LONG).show();
                        }
                        ver_sign.setText("Version 1.0.1");
                    }
                });
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
            if (acct != null) {
                String personName = acct.getDisplayName();
                Uri personPhoto = acct.getPhotoUrl();

                userName.setText(personName);
                ver_sign.setText("Sign Out");

                Picasso.with(MainMenu.this).load(personPhoto.toString())
                        .placeholder(R.drawable.bg_grey)
                        .into(userPicture);

            }
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("Error", "signInResult:failed code=" + e.getStatusCode());
        }
    }
}