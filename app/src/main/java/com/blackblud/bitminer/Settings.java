package com.blackblud.bitminer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;

public class Settings extends AppCompatActivity implements View.OnClickListener {

    GoogleSignInClient mGoogleSignInClient;
    MediaPlayer mp_click;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mp_click = MediaPlayer.create(this, R.raw.click);

        LinearLayout MenuMain = findViewById(R.id.MenuMining);
        LinearLayout MenuPC = findViewById(R.id.MenuPC);
        LinearLayout MenuASIC = findViewById(R.id.MenuASIC);
        LinearLayout MenuSettings = findViewById(R.id.MenuSettings);

        MenuMain.setOnClickListener(this);
        MenuPC.setOnClickListener(this);
        MenuASIC.setOnClickListener(this);
        MenuSettings.setOnClickListener(this);

        Switch switch_music = findViewById(R.id.Switch_music);
        Switch switch_sfx = findViewById(R.id.Switch_sfx);

        ImageView icon_music = findViewById(R.id.ic_music);
        ImageView icon_sfx = findViewById(R.id.ic_sfx);


        switch_music.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mp_click.start();
                save_date();
                if (isChecked) {
                    Data.music = 1;
                    icon_music.setImageResource(R.drawable.settings_music);
                } else {
                    Data.music = 0;
                    icon_music.setImageResource(R.drawable.settings_music_off);
                }
            }
        });

        switch_sfx.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mp_click.start();
                save_date();
                if (isChecked) {
                    Data.sfx = 1;
                    icon_sfx.setImageResource(R.drawable.settings_sfx);
                } else {
                    Data.sfx = 0;
                    icon_sfx.setImageResource(R.drawable.settings_sfx_off);
                }
            }
        });


        if (Data.music == 1) {
            switch_music.setChecked(true);
            icon_music.setImageResource(R.drawable.settings_music);
        } else if (Data.music == 0){
            switch_music.setChecked(false);
            icon_music.setImageResource(R.drawable.settings_music_off);
        }

        if (Data.sfx == 1) {
            switch_sfx.setChecked(true);
            icon_sfx.setImageResource(R.drawable.settings_sfx);
        } else {
            switch_sfx.setChecked(false);
            icon_sfx.setImageResource(R.drawable.settings_sfx_off);
        }
    }

    private void save_date() {
        try {
            Data.SaveData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.MenuMining:
                mp_click.start();
                startActivity(new Intent(Settings.this, MainActivity.class));
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                finish();
                break;
            case R.id.MenuPC:
                mp_click.start();
                startActivity(new Intent(Settings.this, ShopPC.class));
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                finish();
                break;
            case R.id.MenuASIC:
                mp_click.start();
                startActivity(new Intent(Settings.this, ShopASIC.class));
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                finish();
                break;
            case R.id.MenuSettings:
                //startActivity(new Intent(Settings.this, Settings.class));
                //finish();
                break;
        }
    }

    public void SendEmail(View view) {
        mp_click.start();
        Intent mailIntent = new Intent(Intent.ACTION_SEND);
        mailIntent.setType("text/plain");
        mailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"yurii.bezsmolnyi.pz.2018@lpnu.ua"});
        mailIntent.putExtra(Intent.EXTRA_SUBJECT, "BitMiner - Review");
        mailIntent.putExtra(Intent.EXTRA_TEXT, "Cool game :)");

        startActivity(Intent.createChooser(mailIntent, "Send Email"));
    }

    public void RateApp(View view) {
        mp_click.start();
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps")));
    }

    public void ShowTerms(View view) {
        mp_click.start();
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.termsofusegenerator.net/live.php?token=94ZctIKQq9JxSkoPPYaSukURGVb2F0Xp")));
    }

    public void ShowLicences(View view) {
        mp_click.start();
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.gnu.org/licenses/gpl-3.0.uk.html")));
    }

    public void TestMail(View view) {
        try {
            throw new RuntimeException();
        } catch (RuntimeException e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));

            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
            if (acct != null) {
                JavaMailAPI javaMailAPI = new JavaMailAPI(this, "jur20soniic@gmail.com", "Bug Report [" + acct.getEmail() + "]", errors.toString());
                javaMailAPI.execute();
            } else {
                JavaMailAPI javaMailAPI = new JavaMailAPI(this, "jur20soniic@gmail.com", "Bug Report [Anonymous]", errors.toString());
                javaMailAPI.execute();
            }
        }
    }

    public void TestNotification(View view){
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Intent notificationIntent = new Intent(this, AlarmReceiver.class);
        PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 14);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);
    }





}