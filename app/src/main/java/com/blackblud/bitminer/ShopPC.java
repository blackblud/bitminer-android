package com.blackblud.bitminer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import org.w3c.dom.Text;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

public class ShopPC extends AppCompatActivity implements View.OnClickListener {

    TextView os1, os2, os3, os5, os6, os7;
    ImageView os4;
    LinkedList<String> PCUpdatesOS = new LinkedList<String>();

    TextView mb1, mb2, mb3, mb5, mb6, mb7;
    ImageView mb4;
    LinkedList<String> PCUpdatesMB = new LinkedList<String>();

    TextView cpu1, cpu2, cpu3, cpu5, cpu6, cpu7;
    ImageView cpu4;
    LinkedList<String> PCUpdatesCPU = new LinkedList<String>();

    TextView ram1, ram2, ram3, ram5, ram6, ram7;
    ImageView ram4;
    LinkedList<String> PCUpdatesRAM = new LinkedList<String>();

    TextView gpu1, gpu2, gpu3, gpu5, gpu6, gpu7;
    ImageView gpu4;
    LinkedList<String> PCUpdatesGPU = new LinkedList<String>();

    TextView net1, net2, net3, net5, net6, net7;
    ImageView net4;
    LinkedList<String> PCUpdatesNET = new LinkedList<String>();

    int[] Prices = {25, 75, 250, 1150, 5750};
    TextView shop_balance;

    DecimalFormat f_bal = new DecimalFormat("0.00000");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_pc);

        LinearLayout MenuMain = findViewById(R.id.MenuMining);
        LinearLayout MenuPC = findViewById(R.id.MenuPC);
        LinearLayout MenuASIC = findViewById(R.id.MenuASIC);
        LinearLayout MenuSettings = findViewById(R.id.MenuSettings);

        MenuMain.setOnClickListener(this);
        MenuPC.setOnClickListener(this);
        MenuASIC.setOnClickListener(this);
        MenuSettings.setOnClickListener(this);

        LinearLayout shopOS = findViewById(R.id.shop_OS);
        LinearLayout shopMother = findViewById(R.id.shop_Motherboard);
        LinearLayout shopCPU = findViewById(R.id.shop_CPU);
        LinearLayout shopRAM = findViewById(R.id.shop_RAM);
        LinearLayout shopGPU = findViewById(R.id.shop_GraphCard);
        LinearLayout shopNetwork = findViewById(R.id.shop_Network);

        shopOS.setOnClickListener(this);
        shopMother.setOnClickListener(this);
        shopCPU.setOnClickListener(this);
        shopRAM.setOnClickListener(this);
        shopGPU.setOnClickListener(this);
        shopNetwork.setOnClickListener(this);

        shop_balance = findViewById(R.id.shop_balance);
        shop_balance.setText(f_bal.format(Data.balance/100000.00000));

        LinkedListData();
        ViewByID();
        updateHardware();
    }

    private void updateHardware() {
        updateOS();
        updateMotherBoard();
        updateCPU();
        updateRAM();
        updateGPU();
        updateNET();
    }

    private void ViewByID() {
        os1 = findViewById(R.id.OS_1);
        os2 = findViewById(R.id.OS_2);
        os3 = findViewById(R.id.OS_3);
        os4 = findViewById(R.id.OS_img);
        os5 = findViewById(R.id.os_lvl);
        os6 = findViewById(R.id.OS_price);
        os7 = findViewById(R.id.smb_bit_os);

        mb1 = findViewById(R.id.MB_1);
        mb2 = findViewById(R.id.MB_2);
        mb3 = findViewById(R.id.MB_3);
        mb4 = findViewById(R.id.MB_img);
        mb5 = findViewById(R.id.mb_lvl);
        mb6 = findViewById(R.id.MB_price);
        mb7 = findViewById(R.id.smb_bit_mb);

        cpu1 = findViewById(R.id.CPU_1);
        cpu2 = findViewById(R.id.CPU_2);
        cpu3 = findViewById(R.id.CPU_3);
        cpu4 = findViewById(R.id.CPU_img);
        cpu5 = findViewById(R.id.cpu_lvl);
        cpu6 = findViewById(R.id.CPU_price);
        cpu7 = findViewById(R.id.smb_bit_cpu);

        ram1 = findViewById(R.id.RAM_1);
        ram2 = findViewById(R.id.RAM_2);
        ram3 = findViewById(R.id.RAM_3);
        ram4 = findViewById(R.id.RAM_img);
        ram5 = findViewById(R.id.ram_lvl);
        ram6 = findViewById(R.id.RAM_price);
        ram7 = findViewById(R.id.smb_bit_ram);

        gpu1 = findViewById(R.id.GPU_1);
        gpu2 = findViewById(R.id.GPU_2);
        gpu3 = findViewById(R.id.GPU_3);
        gpu4 = findViewById(R.id.GPU_img);
        gpu5 = findViewById(R.id.gpu_lvl);
        gpu6 = findViewById(R.id.GPU_price);
        gpu7 = findViewById(R.id.smb_bit_gpu);

        net1 = findViewById(R.id.NET_1);
        net2 = findViewById(R.id.NET_2);
        net3 = findViewById(R.id.NET_3);
        net4 = findViewById(R.id.NET_img);
        net5 = findViewById(R.id.net_lvl);
        net6 = findViewById(R.id.NET_price);
        net7 = findViewById(R.id.smb_bit_net);
    }

    private void LinkedListData() {
        PCUpdatesOS.add(0, "Win XP");
        PCUpdatesOS.add(1, "Win 7");
        PCUpdatesOS.add(2, "Win 8");
        PCUpdatesOS.add(3, "Win 10");
        PCUpdatesOS.add(4, "HiveOS");
        PCUpdatesOS.add(5, "RaveOS");

        PCUpdatesMB.add(0, "A320");
        PCUpdatesMB.add(1, "A520");
        PCUpdatesMB.add(2, "B450");
        PCUpdatesMB.add(3, "B550");
        PCUpdatesMB.add(4, "X470");
        PCUpdatesMB.add(5, "X570");

        PCUpdatesCPU.add(0, "Ryzen 1600");
        PCUpdatesCPU.add(1, "Ryzen 2600");
        PCUpdatesCPU.add(2, "Ryzen 3600");
        PCUpdatesCPU.add(3, "Ryzen 3900");
        PCUpdatesCPU.add(4, "Ryzen 5800");
        PCUpdatesCPU.add(5, "Ryzen 5950");

        PCUpdatesRAM.add(0, "1x2 GB");
        PCUpdatesRAM.add(1, "2x2 GB");
        PCUpdatesRAM.add(2, "2x4 GB");
        PCUpdatesRAM.add(3, "1x16 GB");
        PCUpdatesRAM.add(4, "2x32 GB");
        PCUpdatesRAM.add(5, "4x64 GB");

        PCUpdatesGPU.add(0, "1060");
        PCUpdatesGPU.add(1, "1070");
        PCUpdatesGPU.add(2, "1080");
        PCUpdatesGPU.add(3, "1660");
        PCUpdatesGPU.add(4, "3080");
        PCUpdatesGPU.add(5, "3090");

        PCUpdatesNET.add(0, "1 Mbit/s");
        PCUpdatesNET.add(1, "5 Mbit/s");
        PCUpdatesNET.add(2, "10 Mbit/s");
        PCUpdatesNET.add(3, "100 Mbit/s");
        PCUpdatesNET.add(4, "1 Gbit/s");
        PCUpdatesNET.add(5, "5 Gbit/s");
    }

    @Override
    public void onClick(View v) {
        MediaPlayer mp_click = MediaPlayer.create(this, R.raw.click);
        switch(v.getId()){
            case R.id.MenuMining:
                mp_click.start();
                startActivity(new Intent(ShopPC.this, MainActivity.class));
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                finish();
                break;
            case R.id.MenuPC:
                //startActivity(new Intent(ShopPC.this, ShopPC.class));
                //finish();
                break;
            case R.id.MenuASIC:
                mp_click.start();
                startActivity(new Intent(ShopPC.this, ShopASIC.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();
                break;
            case R.id.MenuSettings:
                mp_click.start();
                startActivity(new Intent(ShopPC.this, Settings.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();
                break;
            case R.id.shop_OS:
                mp_click.start();
                next_OS();
                save_date();
                break;
            case R.id.shop_Motherboard:
                mp_click.start();
                next_MotherBoard();
                save_date();
                break;
            case R.id.shop_CPU:
                mp_click.start();
                next_CPU();
                save_date();
                break;
            case R.id.shop_RAM:
                mp_click.start();
                next_RAM();
                save_date();
                break;
            case R.id.shop_GraphCard:
                mp_click.start();
                next_GPU();
                save_date();
                break;
            case R.id.shop_Network:
                mp_click.start();
                next_NET();
                save_date();
                break;
        }
    }

    private void save_date() {
        try {
            Data.SaveData();
        } catch (IOException e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));

            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
            if (acct != null) {
                JavaMailAPI javaMailAPI = new JavaMailAPI(this, "jur20soniic@gmail.com", "Bug Report [" + acct.getEmail() + "]", errors.toString());
                javaMailAPI.execute();
            } else {
                JavaMailAPI javaMailAPI = new JavaMailAPI(this, "jur20soniic@gmail.com", "Bug Report [Anonymous]", errors.toString());
                javaMailAPI.execute();
            }
        }
    }


    private void next_OS() {

        if(Data.current_os == 5){
            Toast.makeText(this, "You have reached the maximum level of OS.", Toast.LENGTH_SHORT).show();
            updateOS();
        } else{
            if(Data.balance >= Prices[Data.current_os_price]){
                Data.balance -= Prices[Data.current_os_price];
                shop_balance.setText(f_bal.format(Data.balance/100000.00000));
                Data.current_os += 1;
                Data.current_os_price += 1;
                Data.hash += Data.current_os;
                updateOS();
            } else {
                Toast.makeText(this, "Not enough money on your balance.", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void updateOS() {
        if(Data.current_os == 5){
            os1.setText(PCUpdatesOS.get(Data.current_os));
            os2.setText("");
            os3.setText("");
            os5.setText("" + Data.current_os);
            os4.setImageResource(R.drawable.store_buy_ok);
            os6.setText("✔");
            os7.setText("");
        } else {
            os1.setText(PCUpdatesOS.get(Data.current_os));
            os2.setText(PCUpdatesOS.get(Data.current_os + 1));
            os5.setText("" + Data.current_os);
            os6.setText(f_bal.format(Prices[Data.current_os_price]/100000.00000));
        }
    }

    private void next_MotherBoard() {

        if(Data.current_motherboard == 5){
            Toast.makeText(this, "You have reached the maximum level of Motherboard.", Toast.LENGTH_SHORT).show();
            updateMotherBoard();
        } else{
            if(Data.balance >= Prices[Data.current_motherboard_price]){
                Data.balance -= Prices[Data.current_motherboard_price];
                shop_balance.setText(f_bal.format(Data.balance/100000.00000));
                Data.current_motherboard += 1;
                Data.current_motherboard_price += 1;
                Data.hash += Data.current_motherboard;
                updateMotherBoard();
            } else {
                Toast.makeText(this, "Not enough money on your balance.", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void updateMotherBoard() {
        if(Data.current_motherboard == 5){
            mb1.setText(PCUpdatesMB.get(Data.current_motherboard));
            mb2.setText("");
            mb3.setText("");
            mb5.setText("" + Data.current_motherboard);
            mb4.setImageResource(R.drawable.store_buy_ok);
            mb6.setText("✔");
            mb7.setText("");
        } else {
            mb1.setText(PCUpdatesMB.get(Data.current_motherboard));
            mb2.setText(PCUpdatesMB.get(Data.current_motherboard + 1));
            mb5.setText("" + Data.current_motherboard);
            mb6.setText(f_bal.format(Prices[Data.current_motherboard_price]/100000.00000));
        }
    }

    private void next_CPU() {

        if(Data.current_cpu == 5){
            Toast.makeText(this, "You have reached the maximum level of CPU.", Toast.LENGTH_SHORT).show();
            updateCPU();
        } else{
            if(Data.balance >= Prices[Data.current_cpu_price]){
                Data.balance -= Prices[Data.current_cpu_price];
                shop_balance.setText(f_bal.format(Data.balance/100000.00000));
                Data.current_cpu += 1;
                Data.current_cpu_price += 1;
                Data.hash += Data.current_cpu;
                updateCPU();
            } else {
                Toast.makeText(this, "Not enough money on your balance.", Toast.LENGTH_SHORT).show();
            }
        }

    }
    private void updateCPU() {
        if(Data.current_cpu == 5){
            cpu1.setText(PCUpdatesCPU.get(Data.current_cpu));
            cpu2.setText("");
            cpu3.setText("");
            cpu5.setText("" + Data.current_cpu);
            cpu4.setImageResource(R.drawable.store_buy_ok);
            cpu6.setText("✔");
            cpu7.setText("");
        } else {
            cpu1.setText(PCUpdatesCPU.get(Data.current_cpu));
            cpu2.setText(PCUpdatesCPU.get(Data.current_cpu + 1));
            cpu5.setText("" + Data.current_cpu);
            cpu6.setText(f_bal.format(Prices[Data.current_cpu_price]/100000.00000));
        }
    }

    private void next_RAM() {

        if(Data.current_ram == 5){
            Toast.makeText(this, "You have reached the maximum level of RAM.", Toast.LENGTH_SHORT).show();
            updateRAM();
        } else{
            if(Data.balance >= Prices[Data.current_ram_price]){
                Data.balance -= Prices[Data.current_ram_price];
                shop_balance.setText(f_bal.format(Data.balance/100000.00000));
                Data.current_ram += 1;
                Data.current_ram_price += 1;
                Data.hash += Data.current_ram;
                updateRAM();
            } else {
                Toast.makeText(this, "Not enough money on your balance.", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void updateRAM() {
        if(Data.current_ram == 5){
            ram1.setText(PCUpdatesRAM.get(Data.current_ram));
            ram2.setText("");
            ram3.setText("");
            ram5.setText("" + Data.current_ram);
            ram4.setImageResource(R.drawable.store_buy_ok);
            ram6.setText("✔");
            ram7.setText("");
        } else {
            ram1.setText(PCUpdatesRAM.get(Data.current_ram));
            ram2.setText(PCUpdatesRAM.get(Data.current_ram + 1));
            ram5.setText("" + Data.current_ram);
            ram6.setText(f_bal.format(Prices[Data.current_ram_price]/100000.00000));
        }
    }

    private void next_GPU() {

        if(Data.current_gpu == 5){
            Toast.makeText(this, "You have reached the maximum level of GPU.", Toast.LENGTH_SHORT).show();
            updateGPU();
        } else{
            if(Data.balance >= Prices[Data.current_gpu_price]){
                Data.balance -= Prices[Data.current_gpu_price];
                shop_balance.setText(f_bal.format(Data.balance/100000.00000));
                Data.current_gpu += 1;
                Data.current_gpu_price += 1;
                Data.hash += Data.current_gpu;
                updateGPU();
            } else {
                Toast.makeText(this, "Not enough money on your balance.", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void updateGPU() {
        if(Data.current_gpu == 5){
            gpu1.setText(PCUpdatesGPU.get(Data.current_gpu));
            gpu2.setText("");
            gpu3.setText("");
            gpu5.setText("" + Data.current_gpu);
            gpu4.setImageResource(R.drawable.store_buy_ok);
            gpu6.setText("✔");
            gpu7.setText("");
        } else {
            gpu1.setText(PCUpdatesGPU.get(Data.current_gpu));
            gpu2.setText(PCUpdatesGPU.get(Data.current_gpu + 1));
            gpu5.setText("" + Data.current_gpu);
            gpu6.setText(f_bal.format(Prices[Data.current_gpu_price]/100000.00000));
        }
    }

    private void next_NET() {

        if(Data.current_network == 5){
            Toast.makeText(this, "You have reached the maximum level of Network.", Toast.LENGTH_SHORT).show();
            updateNET();
        } else{
            if(Data.balance >= Prices[Data.current_network_price]){
                Data.balance -= Prices[Data.current_network_price];
                shop_balance.setText(f_bal.format(Data.balance/100000.00000));
                Data.current_network += 1;
                Data.current_network_price += 1;
                Data.hash += Data.current_network;
                updateNET();
            } else {
                Toast.makeText(this, "Not enough money on your balance.", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void updateNET() {
        if(Data.current_network == 5){
            net1.setText(PCUpdatesNET.get(Data.current_network));
            net2.setText("");
            net3.setText("");
            net5.setText("" + Data.current_network);
            net4.setImageResource(R.drawable.store_buy_ok);
            net6.setText("✔");
            net7.setText("");
        } else {
            net1.setText(PCUpdatesNET.get(Data.current_network));
            net2.setText(PCUpdatesNET.get(Data.current_network + 1));
            net5.setText("" + Data.current_network);
            net6.setText(f_bal.format(Prices[Data.current_network_price]/100000.00000));
        }
    }
}