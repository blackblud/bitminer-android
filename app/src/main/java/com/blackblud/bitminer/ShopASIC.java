package com.blackblud.bitminer;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.LinkedList;

public class ShopASIC extends AppCompatActivity implements View.OnClickListener{

    TextView asic11_hash, asic11_lvl, asic11_price, asic11_smb_btc;
    ImageView asic11_img;
    LinkedList<Integer> ASIC1_1 = new LinkedList<>();

    TextView asic12_hash, asic12_lvl, asic12_price, asic12_smb_btc;
    ImageView asic12_img;
    LinkedList<Integer> ASIC1_2 = new LinkedList<>();

    TextView asic13_hash, asic13_lvl, asic13_price, asic13_smb_btc;
    ImageView asic13_img;
    LinkedList<Integer> ASIC1_3 = new LinkedList<>();

    TextView asic21_hash, asic21_lvl, asic21_price, asic21_smb_btc;
    ImageView asic21_img;
    LinkedList<Integer> ASIC2_1 = new LinkedList<>();

    TextView asic22_hash, asic22_lvl, asic22_price, asic22_smb_btc;
    ImageView asic22_img;
    LinkedList<Integer> ASIC2_2 = new LinkedList<>();

    TextView asic23_hash, asic23_lvl, asic23_price, asic23_smb_btc;
    ImageView asic23_img;
    LinkedList<Integer> ASIC2_3 = new LinkedList<>();

    int[] Prices_ASIC = {2499, 4799, 7499, 13299, 24999, 99999};
    TextView shop_balance;

    DecimalFormat f_hash = new DecimalFormat("0.00");
    DecimalFormat f_bal = new DecimalFormat("0.00000");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_asic);

        LinearLayout MenuMain = findViewById(R.id.MenuMining);
        LinearLayout MenuPC = findViewById(R.id.MenuPC);
        LinearLayout MenuASIC = findViewById(R.id.MenuASIC);
        LinearLayout MenuSettings = findViewById(R.id.MenuSettings);

        MenuMain.setOnClickListener(this);
        MenuPC.setOnClickListener(this);
        MenuASIC.setOnClickListener(this);
        MenuSettings.setOnClickListener(this);

        LinearLayout ASIC_1_1 = findViewById(R.id.shop_ASIC_1_1);
        LinearLayout ASIC_1_2 = findViewById(R.id.shop_ASIC_1_2);
        LinearLayout ASIC_1_3 = findViewById(R.id.shop_ASIC_1_3);
        LinearLayout ASIC_2_1 = findViewById(R.id.shop_ASIC_2_1);
        LinearLayout ASIC_2_2 = findViewById(R.id.shop_ASIC_2_2);
        LinearLayout ASIC_2_3 = findViewById(R.id.shop_ASIC_2_3);

        ASIC_1_1.setOnClickListener(this);
        ASIC_1_2.setOnClickListener(this);
        ASIC_1_3.setOnClickListener(this);
        ASIC_2_1.setOnClickListener(this);
        ASIC_2_2.setOnClickListener(this);
        ASIC_2_3.setOnClickListener(this);

        shop_balance = findViewById(R.id.shop_balance_as);
        shop_balance.setText(f_bal.format(Data.balance/100000.00000));

        ViewByIDASIC();
        LinkedListDataASIC();
        updateASICS();
    }

    private void updateASICS() {
        updateASIC_1_1();
        updateASIC_1_2();
        updateASIC_1_3();
        updateASIC_2_1();
        updateASIC_2_2();
        updateASIC_2_3();
    }

    private void ViewByIDASIC() {
        asic11_hash = findViewById(R.id.ASIC1_1_MH);
        asic11_lvl = findViewById(R.id.asic1_1_lvl);
        asic11_price = findViewById(R.id.ASIC1_1_price);
        asic11_smb_btc = findViewById(R.id.smb_bit_ASIC11);
        asic11_img = findViewById(R.id.ASIC11_img);

        asic12_hash = findViewById(R.id.ASIC1_2_MH);
        asic12_lvl = findViewById(R.id.asic1_2_lvl);
        asic12_price = findViewById(R.id.ASIC1_2_price);
        asic12_smb_btc = findViewById(R.id.smb_bit_ASIC12);
        asic12_img = findViewById(R.id.ASIC12_img);

        asic13_hash = findViewById(R.id.ASIC1_3_MH);
        asic13_lvl = findViewById(R.id.asic1_3_lvl);
        asic13_price = findViewById(R.id.ASIC1_3_price);
        asic13_smb_btc = findViewById(R.id.smb_bit_ASIC13);
        asic13_img = findViewById(R.id.ASIC13_img);

        asic21_hash = findViewById(R.id.ASIC2_1_MH);
        asic21_lvl = findViewById(R.id.asic2_1_lvl);
        asic21_price = findViewById(R.id.ASIC2_1_price);
        asic21_smb_btc = findViewById(R.id.smb_bit_ASIC21);
        asic21_img = findViewById(R.id.ASIC21_img);

        asic22_hash = findViewById(R.id.ASIC2_2_MH);
        asic22_lvl = findViewById(R.id.asic2_2_lvl);
        asic22_price = findViewById(R.id.ASIC2_2_price);
        asic22_smb_btc = findViewById(R.id.smb_bit_ASIC22);
        asic22_img = findViewById(R.id.ASIC22_img);

        asic23_hash = findViewById(R.id.ASIC2_3_MH);
        asic23_lvl = findViewById(R.id.asic2_3_lvl);
        asic23_price = findViewById(R.id.ASIC2_3_price);
        asic23_smb_btc = findViewById(R.id.smb_bit_ASIC23);
        asic23_img = findViewById(R.id.ASIC23_img);
    }

    private void LinkedListDataASIC() {
        ASIC1_1.add(0, 0);
        ASIC1_1.add(1, 10);

        ASIC1_2.add(0, 0);
        ASIC1_2.add(1, 25);

        ASIC1_3.add(0, 0);
        ASIC1_3.add(1, 65);

        ASIC2_1.add(0, 0);
        ASIC2_1.add(1, 145);

        ASIC2_2.add(0, 0);
        ASIC2_2.add(1, 325);

        ASIC2_3.add(0, 0);
        ASIC2_3.add(1, 755);
    }

    @Override
    public void onClick(View v) {
        MediaPlayer mp_click = MediaPlayer.create(this, R.raw.click);
        switch(v.getId()){
            case R.id.MenuMining:
                mp_click.start();
                startActivity(new Intent(ShopASIC.this, MainActivity.class));
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                finish();
                break;
            case R.id.MenuPC:
                mp_click.start();
                startActivity(new Intent(ShopASIC.this, ShopPC.class));
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                finish();
                break;
            case R.id.MenuASIC:
                //startActivity(new Intent(ShopASIC.this, ShopASIC.class));
                //finish();
                break;
            case R.id.MenuSettings:
                mp_click.start();
                startActivity(new Intent(ShopASIC.this, Settings.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();
                break;
            case R.id.shop_ASIC_1_1:
                mp_click.start();
                next_ASIC_1_1();
                save_date();
                break;
            case R.id.shop_ASIC_1_2:
                mp_click.start();
                next_ASIC_1_2();
                save_date();
                break;
            case R.id.shop_ASIC_1_3:
                mp_click.start();
                next_ASIC_1_3();
                save_date();
                break;
            case R.id.shop_ASIC_2_1:
                mp_click.start();
                next_ASIC_2_1();
                save_date();
                break;
            case R.id.shop_ASIC_2_2:
                mp_click.start();
                next_ASIC_2_2();
                save_date();
                break;
            case R.id.shop_ASIC_2_3:
                mp_click.start();
                next_ASIC_2_3();
                save_date();
                break;
        }
    }

    private void save_date() {
        try {
            Data.SaveData();
        } catch (IOException e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));

            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
            if (acct != null) {
                JavaMailAPI javaMailAPI = new JavaMailAPI(this, "jur20soniic@gmail.com", "Bug Report [" + acct.getEmail() + "]", errors.toString());
                javaMailAPI.execute();
            } else {
                JavaMailAPI javaMailAPI = new JavaMailAPI(this, "jur20soniic@gmail.com", "Bug Report [Anonymous]", errors.toString());
                javaMailAPI.execute();
            }
        }
    }

    private void next_ASIC_1_1() {

        if(Data.current_asic_1_1 == 1){
            Toast.makeText(this, "You have reached the maximum level of ASIC 1-1.", Toast.LENGTH_SHORT).show();
            updateASIC_1_1();
        } else{
            if(Data.balance >= Prices_ASIC[0]){
                Data.balance -= Prices_ASIC[Data.current_asic_1_1_price];
                shop_balance.setText(f_bal.format(Data.balance/100000.00000));
                Data.current_asic_1_1 += 1;
                Data.auto_hash += Integer.parseInt((String) asic11_hash.getText());
                updateASIC_1_1();
            } else {
                Toast.makeText(this, "Not enough money on your balance.", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void updateASIC_1_1() {
        if(Data.current_asic_1_1 == 1){
            asic11_lvl.setText("" + Data.current_asic_1_1);
            asic11_img.setImageResource(R.drawable.store_buy_ok);
            asic11_price.setText("✔");
            asic11_smb_btc.setText("");
        } else {
            asic11_lvl.setText("" + Data.current_asic_1_1);
            asic11_price.setText(f_bal.format(Prices_ASIC[0]/100000.00000));
        }
    }

    private void next_ASIC_1_2() {

        if(Data.current_asic_1_2 == 1){
            Toast.makeText(this, "You have reached the maximum level of ASIC 1-2.", Toast.LENGTH_SHORT).show();
            updateASIC_1_2();
        } else{
            if(Data.balance >= Prices_ASIC[1]){
                Data.balance -= Prices_ASIC[Data.current_asic_1_2_price];
                shop_balance.setText(f_bal.format(Data.balance/100000.00000));
                Data.current_asic_1_2 += 1;
                Data.auto_hash += Integer.parseInt((String) asic12_hash.getText());
                updateASIC_1_2();
            } else {
                Toast.makeText(this, "Not enough money on your balance.", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void updateASIC_1_2() {
        if(Data.current_asic_1_2 == 1){
            asic12_lvl.setText("" + Data.current_asic_1_2);
            asic12_img.setImageResource(R.drawable.store_buy_ok);
            asic12_price.setText("✔");
            asic12_smb_btc.setText("");
        } else {
            asic12_lvl.setText("" + Data.current_asic_1_2);
            asic12_price.setText(f_bal.format(Prices_ASIC[1]/100000.00000));
        }
    }

    private void next_ASIC_1_3() {

        if(Data.current_asic_1_3 == 1){
            Toast.makeText(this, "You have reached the maximum level of ASIC 1-3.", Toast.LENGTH_SHORT).show();
            updateASIC_1_3();
        } else{
            if(Data.balance >= Prices_ASIC[2]){
                Data.balance -= Prices_ASIC[Data.current_asic_1_3_price];
                shop_balance.setText(f_bal.format(Data.balance/100000.00000));
                Data.current_asic_1_3 += 1;
                Data.auto_hash += Integer.parseInt((String) asic13_hash.getText());
                updateASIC_1_3();
            } else {
                Toast.makeText(this, "Not enough money on your balance.", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void updateASIC_1_3() {
        if(Data.current_asic_1_3 == 1){
            asic13_lvl.setText("" + Data.current_asic_1_3);
            asic13_img.setImageResource(R.drawable.store_buy_ok);
            asic13_price.setText("✔");
            asic13_smb_btc.setText("");
        } else {
            asic13_lvl.setText("" + Data.current_asic_1_3);
            asic13_price.setText(f_bal.format(Prices_ASIC[2]/100000.00000));
        }
    }

    private void next_ASIC_2_1() {

        if(Data.current_asic_2_1 == 1){
            Toast.makeText(this, "You have reached the maximum level of ASIC 2-1.", Toast.LENGTH_SHORT).show();
            updateASIC_2_1();
        } else{
            if(Data.balance >= Prices_ASIC[3]){
                Data.balance -= Prices_ASIC[Data.current_asic_2_1_price];
                shop_balance.setText(f_bal.format(Data.balance/100000.00000));
                Data.current_asic_2_1 += 1;
                Data.auto_hash += Integer.parseInt((String) asic21_hash.getText());
                updateASIC_2_1();
            } else {
                Toast.makeText(this, "Not enough money on your balance.", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void updateASIC_2_1() {
        if(Data.current_asic_2_1 == 1){
            asic21_lvl.setText("" + Data.current_asic_2_1);
            asic21_img.setImageResource(R.drawable.store_buy_ok);
            asic21_price.setText("✔");
            asic21_smb_btc.setText("");
        } else {
            asic21_lvl.setText("" + Data.current_asic_2_1);
            asic21_price.setText(f_bal.format(Prices_ASIC[3]/100000.00000));
        }
    }

    private void next_ASIC_2_2() {

        if(Data.current_asic_2_2 == 1){
            Toast.makeText(this, "You have reached the maximum level of ASIC 2-2.", Toast.LENGTH_SHORT).show();
            updateASIC_2_2();
        } else{
            if(Data.balance >= Prices_ASIC[4]){
                Data.balance -= Prices_ASIC[Data.current_asic_2_2_price];
                shop_balance.setText(f_bal.format(Data.balance/100000.00000));
                Data.current_asic_2_2 += 1;
                Data.auto_hash += Integer.parseInt((String) asic22_hash.getText());
                updateASIC_2_2();
            } else {
                Toast.makeText(this, "Not enough money on your balance.", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void updateASIC_2_2() {
        if(Data.current_asic_2_2 == 1){
            asic22_lvl.setText("" + Data.current_asic_2_2);
            asic22_img.setImageResource(R.drawable.store_buy_ok);
            asic22_price.setText("✔");
            asic22_smb_btc.setText("");
        } else {
            asic22_lvl.setText("" + Data.current_asic_2_2);
            asic22_price.setText(f_bal.format(Prices_ASIC[4]/100000.00000));
        }
    }

    private void next_ASIC_2_3() {

        if(Data.current_asic_2_3 == 1){
            Toast.makeText(this, "You have reached the maximum level of ASIC 2-3.", Toast.LENGTH_SHORT).show();
            updateASIC_2_3();
        } else{
            if(Data.balance >= Prices_ASIC[5]){
                Data.balance -= Prices_ASIC[Data.current_asic_2_3_price];
                shop_balance.setText(f_bal.format(Data.balance/100000.00000));
                Data.current_asic_2_3 += 1;
                Data.auto_hash += Integer.parseInt((String) asic23_hash.getText());
                updateASIC_2_3();
            } else {
                Toast.makeText(this, "Not enough money on your balance.", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void updateASIC_2_3() {
        if(Data.current_asic_2_3 == 1){
            asic23_lvl.setText("" + Data.current_asic_2_3);
            asic23_img.setImageResource(R.drawable.store_buy_ok);
            asic23_price.setText("✔");
            asic23_smb_btc.setText("");
        } else {
            asic23_lvl.setText("" + Data.current_asic_2_3);
            asic23_price.setText(f_bal.format(Prices_ASIC[5]/100000.00000));
        }
    }
}