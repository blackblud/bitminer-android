package com.blackblud.bitminer;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;

import org.w3c.dom.Text;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.Set;

public class MainActivity extends Activity implements View.OnClickListener {

    TextView TVbalance;
    TextView TVhash;
    TextView TVautohash;
    TextView TVlevelnumber;
    ProgressBar PBlevel_bar;

    GoogleSignInClient mGoogleSignInClient;

    DecimalFormat f_bal = new DecimalFormat("0.00000");
    DecimalFormat f_hash = new DecimalFormat("0.00");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LinearLayout MenuMain = findViewById(R.id.MenuMining);
        LinearLayout MenuPC = findViewById(R.id.MenuPC);
        LinearLayout MenuASIC = findViewById(R.id.MenuASIC);
        LinearLayout MenuSettings = findViewById(R.id.MenuSettings);
        LinearLayout BitcoinClick = findViewById(R.id.BitcoinClick);

        MenuMain.setOnClickListener(this);
        MenuPC.setOnClickListener(this);
        MenuASIC.setOnClickListener(this);
        MenuSettings.setOnClickListener(this);
        BitcoinClick.setOnClickListener(this);

        TVlevelnumber = findViewById(R.id.levelnumber);
        PBlevel_bar = findViewById(R.id.LVLprogressBar);
        TVbalance = findViewById(R.id.balance_value);
        TVhash = findViewById(R.id.hash_value);
        TVautohash = findViewById(R.id.auto_hash_value);

        TVlevelnumber.setText("" + Data.level);
        PBlevel_bar.setProgress(Data.progress);
        TVbalance.setText(f_bal.format(Data.balance/100000.00000));
        TVhash.setText("" + Data.hash);
        TVhash.setText(f_hash.format(Data.hash));
        TVautohash.setText(f_hash.format(Data.auto_hash));

        //balance = (int) Double.parseDouble(TVbalance.getText().toString());
        //hash = (int) Double.parseDouble(TVhash.getText().toString());


    }



    @Override
    public void onClick(View v) {
        MediaPlayer mp_click = MediaPlayer.create(MainActivity.this, R.raw.click);
        MediaPlayer mp_coin = MediaPlayer.create(MainActivity.this, R.raw.coin);
        switch(v.getId()){
            case R.id.MenuMining:
                if(Data.sfx == 1){ mp_click.start(); }
                //startActivity(new Intent(MainActivity.this, MainActivity.class));
                //finish();
                break;
            case R.id.MenuPC:
                if(Data.sfx == 1){ mp_click.start(); }
                startActivity(new Intent(MainActivity.this, ShopPC.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();
                break;
            case R.id.MenuASIC:
                if(Data.sfx == 1){ mp_click.start(); }
                startActivity(new Intent(MainActivity.this, ShopASIC.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();
                break;
            case R.id.MenuSettings:
                if(Data.sfx == 1){ mp_click.start(); }
                startActivity(new Intent(MainActivity.this, Settings.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();
                break;
            case R.id.BitcoinClick:
                if(Data.sfx == 1){ mp_coin.start(); }
                LetsMine();
                try {
                    Data.SaveData();
                } catch (IOException e) {
                    StringWriter errors = new StringWriter();
                    e.printStackTrace(new PrintWriter(errors));

                    GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
                    if (acct != null) {
                        JavaMailAPI javaMailAPI = new JavaMailAPI(this, "jur20soniic@gmail.com", "Bug Report [" + acct.getEmail() + "]", errors.toString());
                        javaMailAPI.execute();
                    } else {
                        JavaMailAPI javaMailAPI = new JavaMailAPI(this, "jur20soniic@gmail.com", "Bug Report [Anonymous]", errors.toString());
                        javaMailAPI.execute();
                    }
                }
                break;
        }
    }

    public void LetsMine() {
        Data.level = Integer.parseInt((String) TVlevelnumber.getText());
        Data.progress = PBlevel_bar.getProgress();

        Data.balance += Data.hash;
        TVbalance.setText(f_bal.format(Data.balance/100000.00000));


        String value = (String)TVlevelnumber.getText();
        int int_value = Integer.parseInt(value);

        if (int_value >= 100){
            PBlevel_bar.setProgress(100);
            Toast.makeText(this, "You reached the highest level. Congratulations!", Toast.LENGTH_LONG).show();
        } else if(PBlevel_bar.getProgress() == 100){
            PBlevel_bar.setProgress(1);
            int_value += 1;
            TVlevelnumber.setText("" + int_value);
        } else{
            PBlevel_bar.setProgress(PBlevel_bar.getProgress() + 1);
        }
    }

    public void ChangeHash(View view) {
        Data.hash += 1;
        TVhash.setText(f_hash.format(Data.hash));
    }
}