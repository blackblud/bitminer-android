package com.blackblud.bitminer;

import android.app.Activity;
import android.app.Application;
import android.os.Environment;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;

public class Data {
    static public int music;
    static public int sfx;

    static public int level;
    static public int progress;
    static public int balance;
    static public int hash;
    static public int auto_hash;

    static public int current_os;
    static public int current_os_price;

    static public int current_motherboard;
    static public int current_motherboard_price;

    static public int current_cpu;
    static public int current_cpu_price;

    static public int current_ram;
    static public int current_ram_price;

    static public int current_gpu;
    static public int current_gpu_price;

    static public int current_network;
    static public int current_network_price;

    static public int current_asic_1_1;
    static public int current_asic_1_1_price;

    static public int current_asic_1_2;
    static public int current_asic_1_2_price;

    static public int current_asic_1_3;
    static public int current_asic_1_3_price;

    static public int current_asic_2_1;
    static public int current_asic_2_1_price;

    static public int current_asic_2_2;
    static public int current_asic_2_2_price;

    static public int current_asic_2_3;
    static public int current_asic_2_3_price;

    public static String path = "/storage/emulated/0/Android/Data/com.blackblud.bitminer";

    static public void SaveData() throws IOException {
        DecimalFormat f_bal = new DecimalFormat("0.00000");
        DecimalFormat f_hash = new DecimalFormat("0.00");
        File file = new File(path + "/SaveFile.txt");
        Date currentTime = Calendar.getInstance().getTime();
        String text =   "Setting" + "\n\t" +
                        "Music - " + music + "\n\t" +
                        "SFX - " + sfx + "\n\n" +


                        "User Info" + "\n\t" +
                        "Level - " + level + "\n\t" +
                        "Progress - " + progress + "\n\t" +
                        "Balance - " + f_bal.format(balance/100000.00000) + "\n\t" +
                        "Hash - " + hash + ".00\n\t" +
                        "AutoHash - " + auto_hash + "\n\n" +

                        "PC Info" + "\n\t" +
                        "OS - " + current_os + "\n\t" +
                        "MotherBoard - " + current_motherboard + "\n\t" +
                        "CPU - " + current_cpu + "\n\t" +
                        "RAM - " + current_ram + "\n\t" +
                        "GPU - " + current_gpu + "\n\t" +
                        "Network - " + current_network + "\n\n" +

                        "ASIC Info" + "\n\t" +
                        "ASIC 1_1 - " + current_asic_1_1 + "\n\t" +
                        "ASIC 1_2 - " + current_asic_1_2 + "\n\t" +
                        "ASIC 1_3 - " + current_asic_1_3 + "\n\t" +
                        "ASIC 2_1 - " + current_asic_2_1 + "\n\t" +
                        "ASIC 2_2 - " + current_asic_2_2 + "\n\t" +
                        "ASIC 2_3 - " + current_asic_2_3 + "\n\n" +

                        "Last Saved [" + currentTime +"]\n";


        FileOutputStream fos = null;
        fos = new FileOutputStream(file);
        fos.write(text.getBytes());
        fos.close();

        saveFULLDate();
    }

    private static void saveFULLDate() throws IOException {
        File fileFULL = new File(path + "/SaveFile1.txt");
        String text =
                level + "|" +
                progress + "|" +
                balance + "|" +
                hash + "|" +
                auto_hash + "|" +

                current_os + "|" +
                current_motherboard + "|" +
                current_cpu + "|" +
                current_ram + "|" +
                current_gpu + "|" +
                current_network + "|" +

                current_os_price + "|" +
                current_motherboard_price + "|" +
                current_cpu_price + "|" +
                current_ram_price + "|" +
                current_gpu_price + "|" +
                current_network_price + "|" +

                current_asic_1_1 + "|" +
                current_asic_1_1_price + "|" +
                current_asic_1_2 + "|" +
                current_asic_1_2_price + "|" +
                current_asic_1_3 + "|" +
                current_asic_1_3_price + "|" +
                current_asic_2_1 + "|" +
                current_asic_2_1_price + "|" +
                current_asic_2_2 + "|" +
                current_asic_2_2_price + "|" +
                current_asic_2_3 + "|" +
                current_asic_2_3_price + "|" +

                music + "|" +
                sfx;


        FileOutputStream fos = null;
        fos = new FileOutputStream(fileFULL);
        fos.write(text.getBytes());
        fos.close();
    }

}
